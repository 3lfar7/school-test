# HOW TO
## Download
![01.jpg](docs/01.jpg)
![02.jpg](docs/02.jpg)

## Help
* Скачать https://code.visualstudio.com/
* index.html - точка входа, открывать в браузере (Chrome).
* styles.css - стили.
* script.js - основной код.
* config.js - файл конфигурации (таймер, справка, вопросы). Сюда добавлять вопросы.
* test - папка с ресурсами по предметам (картинки).
 