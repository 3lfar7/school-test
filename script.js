'use strict';

function render(el) {
  let appEl = document.getElementById('app');
  appEl.innerHTML = '';
  if (el) {
    appEl.appendChild(el);
  }
}

class Button {
  constructor (text, clickFunc) {
    this.el = document.createElement('div');
    this.el.classList.add('button');
    this.el.innerHTML = text;
    if (clickFunc) {
      this.el.addEventListener('click', clickFunc);
    }
  }
}

class Timer {
  constructor (time, overFunc) {
    this.time = time;
    this.el = document.createElement('div');
    this.el.classList.add('timer');
    this.el.innerHTML = this.formattedTime;
    
    let startTime = Date.now();
    this._intervalId = setInterval(() => {
      let currTime = Date.now();
      this.time = this.time - (currTime - startTime);
      if (this.time < 0) {
        this.time = 0;
      }
      startTime = currTime;
      this.el.innerHTML = this.formattedTime;
      if (this.time === 0) {
        this.stop();
        if (overFunc) {
          overFunc();
        }
      }
    }, 200);
  }

  stop () {
    clearInterval(this._intervalId);
  }

  get formattedTime () {
    let time = Math.floor(this.time / 1000);
    let hours = Math.floor(time / 3600);
    let minutes = Math.floor((time % 3600) / 60);
    let seconds = Math.floor(time % 60);
    hours = hours < 10 ? `0${hours}` : `${hours}`;
    minutes = minutes < 10 ? `0${minutes}` : `${minutes}`;
    seconds = seconds < 10 ? `0${seconds}` : `${seconds}`;
    return `${hours}:${minutes}:${seconds}`;
  }
}

class MainMenu {
  constructor () {
    this.el = document.createElement('div');
    this.el.classList.add('main-menu');
    this.el.appendChild(this._createSubjectList());
  }

  _createMenuItem(title, clickFunc) {
    let button = new Button(title, clickFunc);
    button.el.classList.add('main-menu__item');
    return button.el;
  }

  _createVariantList (subject) {
    let el = document.createDocumentFragment();
    for (let [i, variant] of subject.variants.entries()) {
      el.appendChild(this._createMenuItem(`Варіант ${i + 1}`, () => {
        let test = new Test(variant);
        render(test.el);
      }));
    }
    el.appendChild(this._createMenuItem('Повернутися', () => {
      this.el.innerHTML = '';
      this.el.appendChild(this._createSubjectList());
    }));
    return el;
  }

  _createSubjectList () {
    let el = document.createDocumentFragment();
    for (let subject of TEST) {
      el.appendChild(this._createMenuItem(subject.name, () => {
        this.el.innerHTML = '';
        this.el.appendChild(this._createVariantList(subject));
      }));
    }
    el.appendChild(this._createMenuItem('Довідка', () => {
      let info = new Info();
      render(info.el);
    }));
    return el;
  }
}

class Test {
  constructor (variant) {
    this.variant = variant;
    this._finished = false;
    this.el = document.createElement('div');
    this.el.classList.add('test');
    this.el.appendChild(this._createHeader());
    this.el.appendChild(this._createItemList());
  }

  _createHeader () {
    let el = document.createElement('div');
    el.classList.add('test__header');
    let returnButton = new Button('Повернутися', () => {
      let isReturn = true;
      if (!this._finished) {
        isReturn = confirm('Ви не закінчили тест. Ви впевнені, що хочете повернутися?');
      }
      if (isReturn) {
        this.clear();
        let mainMenu = new MainMenu();
        render(mainMenu.el);
      }
    });
    returnButton.el.classList.add('test__return-button');
    let timer = new Timer(TIMER_DURATION, () => {
      this._finish();
    });
    this._timer = timer;
    timer.el.classList.add('test__timer');
    let finishButton = new Button('Закінчити', () => {
      let inputs = document.querySelectorAll('input');
      let checkedCount = 0;
      for (let input of inputs) {
        if (input.checked) {
          checkedCount++;
        }
      }
      let isFinish = true;
      if (this.variant.length !== checkedCount) {
        isFinish = confirm('Ви не відповіли на усі запитання. Ви впевнені, що хочете закінчити?');
      }
      if (isFinish) {
        timer.stop();
        this._finish();
      }
    });
    finishButton.el.classList.add('test__finish-button');
    el.appendChild(returnButton.el);
    el.appendChild(timer.el);
    el.appendChild(finishButton.el);
    return el;
  }

  _createItem (index, item) {
    let el = document.createElement('div');
    el.classList.add('test__item');

    let questionEl = document.createElement('div');
    questionEl.classList.add('test__question');
    questionEl.innerHTML = `${index + 1}. ${item.question}`;
    el.appendChild(questionEl);

    let answerListEl = document.createElement('div');
    answerListEl.classList.add('test__answer-list');
    for (let [answerIndex, answerText] of item.answers.entries()) {
      let answerEl = document.createElement('label');
      answerEl.classList.add('test__answer');
      answerEl.innerHTML = `<input type="radio" name="question-${index}" value="${answerIndex}"> ${answerText}`;
      answerListEl.appendChild(answerEl);
    }
    el.appendChild(answerListEl);

    return el;
  }

  _createItemList () {
    let el = document.createElement('div');
    el.classList.add('test__item-list');
    for (let [index, item] of this.variant.entries()) {
      el.append(this._createItem(index, item));
    }
    return el;
  }

  _createResult (correctCount) {
    let el = document.createElement('div');
    el.classList.add('test__result');
    el.innerHTML = `${correctCount}/${this.variant.length}`;
    return el;
  }

  _finish () {
    let finishButton = document.querySelector('.test__finish-button');
    finishButton.remove();
    let answers = document.querySelectorAll('.test__answer-list');
    let correctCount = 0;
    for (let [i, answerListEl] of answers.entries()) {
      let inputs = answerListEl.querySelectorAll('input');
      for (let inputEl of inputs) {
        inputEl.disabled = true;
        if (inputEl.checked) {
          if (inputEl.value == this.variant[i].correctAnswer) {
            inputEl.parentElement.classList.add('test__answer--correct');
            correctCount++;
          } else {
            inputEl.parentElement.classList.add('test__answer--incorrect');
          }
        }
      }
    }
    let header = document.querySelector('.test__header');
    header.appendChild(this._createResult(correctCount));
    this._finished = true;
  }

  clear () {
    this._timer.stop();
  }
}

class Info {
  constructor () {
    this.el = document.createElement('div');
    this.el.classList.add('info');
    let headerEl = document.createElement('h1');
    headerEl.classList.add('info__header');
    headerEl.innerHTML = 'Довідка';
    let contentEl = document.createElement('div');
    contentEl.classList.add('info__content');
    contentEl.innerHTML = INFO;
    let button = new Button('Повернутися', () => {
      let mainMenu = new MainMenu();
      render(mainMenu.el);
    });
    button.el.classList.add('info__return-button');
    this.el.appendChild(headerEl);
    this.el.appendChild(contentEl);
    this.el.appendChild(button.el);
  }
}

document.addEventListener('DOMContentLoaded', function () {
  let mainMenu = new MainMenu();
  render(mainMenu.el);
});